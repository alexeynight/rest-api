<?php

use Component\{Request};
use Service\RouterService;

session_start();

require 'src/Autoloader.php';
spl_autoload_register('Autoloader::loader');

$request = Request::create();

$routerServise = new RouterService();
$router = $routerServise->createRouter();

$parameters = $router->match($request->getPathInfo());
$request->setAttributes((object)$parameters);

$kernel = new Kernel();
$response = $kernel->handle($request);
$kernel->terminate($response);