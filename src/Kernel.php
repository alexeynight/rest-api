<?php

use Component\{
    ControllerResolver, ArgumentResolver, Request, Response
};

/**
 * Class Kernel
 *
 * @author Alexey Fedko <alexey.night@gmail.com>
 */
class Kernel
{

    /**
     * @var ControllerResolver
     */
    private $resolver;
    /**
     * @var ArgumentResolver
     */
    private $argumentResolver;

    /**
     * Kernel constructor.
     */
    public function __construct()
    {
        // in real application we need use injection
        $this->resolver = new ControllerResolver();
        $this->argumentResolver = new ArgumentResolver();
    }

    /**
     * @param Request $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle(Request $request)
    {
        // get controller
        if (false === $controller = $this->resolver->getController($request)) {
            throw new Exception(sprintf('Unable to find the controller.'));
        }

        // get arguments
        $arguments = $this->argumentResolver->getArguments($request, $controller);

        // call controller
        return \call_user_func_array($controller, $arguments);
    }

    /**
     * @param Response $response
     */
    public function terminate(Response $response)
    {
        header("HTTP/1.1 ".$response->getStatusCode().' '.$response->getStatusText());

        foreach ($response->getHeaders() as $headerKey => $headerValue) {
            header($headerKey.': '.$headerValue);
        }
        echo $response->getContent();
        exit(0);
    }
}