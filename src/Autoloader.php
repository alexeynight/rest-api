<?php

class Autoloader
{

    /**
     * File extension as a string. Defaults to ".php".
     */
    protected static $fileExt = '.php';
    /**
     * The topmost directory where recursion should begin.
     * Defaults to the current directory.
     */
    protected static $pathTop = __DIR__;
    /**
     * A placeholder to hold the file iterator so that
     * directory traversal is only performed once.
     */
    protected static $fileIterator = null;

    /**
     * Autoload function for registration with
     * spl_autoload_register
     *
     * Looks recursively through project directory and
     * loads class files based on filename match.
     *
     * @param string $className
     */
    public static function loader($className)
    {
        $pathInfo = static::getPathInfo($className);

        $directory = new RecursiveDirectoryIterator($pathInfo->dirname, RecursiveDirectoryIterator::SKIP_DOTS);
        $fileIterator = new RecursiveIteratorIterator($directory);

        foreach ($fileIterator as $file) {
            if (strtolower($file->getFilename()) === strtolower($pathInfo->basename)) {
                if ($file->isReadable()) {
                    include_once $file->getPathname();
                }
                break;
            }
        }
    }

    /**
     * Sets the $fileExt property
     *
     * @param string $fileExt
     */
    public static function setFileExt($fileExt)
    {
        static::$fileExt = $fileExt;
    }

    /**
     * Sets the $path property
     *
     * @param string $path
     */
    public static function setPath($path)
    {
        static::$pathTop = $path;
    }

    /**
     * @param $className
     *
     * @return object
     */
    private static function getPathInfo($className)
    {
        $path = static::$pathTop.DIRECTORY_SEPARATOR.str_replace('\\', DIRECTORY_SEPARATOR, $className).static::$fileExt;

        return (object)pathinfo($path);
    }
}