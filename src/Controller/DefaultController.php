<?php

namespace Controller;

use Service\AuthService;
use Component\{
    Request, Response, UnauthorizedResponse, RedirectResponse
};

/**
 * Class DefaultController
 *
 * @package Controller
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class DefaultController
{
    /** @var AuthService */
    private $authService;

    /**
     * Auth constructor.
     */
    public function __construct()
    {
        $this->authService = new AuthService();
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->authService->isLoggedIn($request) ? new RedirectResponse('/api/users/') : new UnauthorizedResponse();
    }
}
