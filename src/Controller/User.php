<?php

namespace Controller;

use Service\{
    AuthService, UserService
};
use Component\{
    Request, Response, JsonResponse, UnauthorizedResponse
};

/**
 * Class User
 *
 * @author Alexey Fedko <alexey.night@gmail.com>
 */
class User
{

    /** @var UserService */
    private $userService;

    /** @var UserService */
    private $authService;

    /**
     * User constructor.
     */
    public function __construct()
    {
        // we need use DI
        $this->userService = new UserService();
        $this->authService = new AuthService();
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return Response
     */
    public function getOneAction(Request $request, $id): Response
    {
        if (!$this->authService->isLoggedIn($request)) {
            return new UnauthorizedResponse();
        }

        $user = $this->userService->getUserById($id);

        return new JsonResponse(['data' => $user]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function listAction(Request $request): Response
    {
        if (!$this->authService->isLoggedIn($request)) {
            return new UnauthorizedResponse();
        }

        $users = $this->userService->getAllUsers();

        return new JsonResponse(
            [
                'itemsCount' => count($users),
                'data'       => $users,
            ]
        );
    }

}
