<?php

namespace Component;

/**
 * Class UnauthorizedResponse
 *
 * @package Component
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class UnauthorizedResponse extends Response
{

    /**
     * RedirectResponse constructor.
     *
     * @param string $content
     * @param int    $status
     * @param array  $headers
     */
    public function __construct($content = '', $status = self::HTTP_UNAUTHORIZED, array $headers = [])
    {
        parent::__construct('', $status, $headers);

        $this->addHeader('WWW-Authenticate', 'Basic realm="My Realm"');
    }
}
