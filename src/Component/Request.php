<?php

namespace Component;

use \stdClass;

/**
 * Class Request
 *
 * @author Alexey Fedko <alexey.night@gmail.com>
 */
class Request
{

    private $headers;
    private $server;
    private $request;
    private $query;
    private $content;
    private $cookies;
    private $attributes;

    private $pathInfo;
    private $requestMethod;

    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';

    // we can add more methods

    /**
     * Request constructor.
     *
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $server
     * @param null  $content
     * @param array $headers
     */
    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $server = [], $content = null, $headers = [])
    {
        $this->request = (object)$request;
        $this->query = (object)$query;
        $this->content = (object)$content;
        $this->attributes = (object)$attributes;
        $this->cookies = (object)$cookies;
        $this->content = (object)$content;
        $this->headers = (object)$headers;
        $this->server = (object)$server;

        $this->pathInfo = $server['PATH_INFO'];
        $this->requestMethod = $server['REQUEST_METHOD'];
    }

    /**
     * @return static
     */
    public static function create()
    {
        $request = new static($_GET, $_POST, [], $_COOKIE, $_SERVER);

        if (0 === strpos($request->headers->{'CONTENT_TYPE'}, 'application/x-www-form-urlencoded') && in_array(
                strtoupper($request->server->{'REQUEST_METHOD'}), [
                    'PUT',
                    'DELETE',
                ]
            )) {
            $putData = file_get_contents('php://input');;
            parse_str($putData, $data);
            $request->request = (object)$data;
        }

        return $request;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return mixed
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @return stdClass
     */
    public function getRequest(): stdClass
    {
        return $this->request;
    }

    /**
     * @return stdClass
     */
    public function getQuery(): stdClass
    {
        return $this->query;
    }

    /**
     * @return stdClass
     */
    public function getContent(): stdClass
    {
        return $this->content;
    }

    /**
     * @return stdClass
     */
    public function getCookies(): stdClass
    {
        return $this->cookies;
    }

    /**
     * @return stdClass
     */
    public function getAttributes(): stdClass
    {
        return $this->attributes;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function addAttribute($key, $value)
    {
        $this->attributes->$key = $value;
    }

    /**
     * @param stdClass $attributes
     */
    public function setAttributes(stdClass $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return mixed
     */
    public function getRequestMethod()
    {
        return $this->requestMethod;
    }

    /**
     * @return mixed
     */
    public function getPathInfo()
    {
        return $this->pathInfo;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
