<?php

namespace Component;

/**
 * Class ControllerResolver
 *
 * @package Component
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class ControllerResolver
{

    const CONTROLLER_NAMESPACE = 'Controller';
    const ACTION_POSTFIX = 'Action';

    public function getController(Request $request)
    {
        $controllerName = $this->getControllerName($request);
        $methodName = $this->getMethodName($request);

        return $this->createController($controllerName, $methodName);
    }

    /**
     * Returns a callable for the given controller.
     *
     * @param string $controllerName A Controller string
     * @param string $methodName     A Method string
     *
     * @return callable
     *
     * @throws \InvalidArgumentException
     */
    protected function createController($controllerName, $methodName)
    {
        if (!class_exists($controllerName)) {
            throw new \InvalidArgumentException(sprintf('Controller "%s" does not exist.', $controllerName));
        }

        return [new $controllerName, $methodName];
    }

    /**
     * @param Request $request
     *
     * @return string
     *
     * @throws \Exception
     */
    private function getControllerName(Request $request)
    {
        if (!$controller = $request->getAttributes()->{'controller'}) {
            throw new \Exception(sprintf('Unable to find the method of controller.'));
        }

        return self::CONTROLLER_NAMESPACE.'\\'.$controller;
    }

    /**
     * @param Request $request
     *
     * @return string
     *
     * @throws \Exception
     */
    private function getMethodName(Request $request)
    {
        if (!$method = $request->getAttributes()->{'method'}) {
            throw new \Exception(sprintf('Unable to find the method of controller.'));
        }

        return $method.self::ACTION_POSTFIX;
    }
}
