<?php

namespace Component;

/**
 * Class DataProvider
 *
 * @package Component
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class DataProvider
{

    const DATA_DIRECTORY = 'data';

    /**
     * @param string $filePath
     *
     * @return array
     */
    public function getData($filePath)
    {
        $usersData = [];

        if (($handle = fopen(getcwd().DIRECTORY_SEPARATOR.self::DATA_DIRECTORY.DIRECTORY_SEPARATOR.$filePath, "r")) !== false) {
            if (($data = fgetcsv($handle, 1000, ",")) === false) {
                return [];
            }

            $dataKeys = $data;
            $rowsCount = count($data);

            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $line = [];
                for ($i = 0; $i < $rowsCount; $i++) {
                    $line[$dataKeys[$i]] = $data[$i];
                }
                $usersData[$line[$dataKeys[0]]] = $line;
            }
            fclose($handle);
        }

        return $usersData;
    }
}
