<?php

namespace Component;

/**
 * Class RedirectResponse
 *
 * @package Component
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class RedirectResponse extends Response
{

    /**
     * RedirectResponse constructor.
     *
     * @param string $content
     * @param int    $status
     * @param array  $headers
     */
    public function __construct($content = '', $status = self::HTTP_MOVED_PERMANENTLY, array $headers = [])
    {
        parent::__construct('', $status, $headers);

        $this->addHeader('Location', $content);
    }
}
