<?php

namespace Component;

/**
 * Class Router
 *
 * @package Component
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class Router
{
    /** @var array  */
    private $routes = [];

    /**
     * @param $routeData
     */
    public function add($routeData)
    {
        $this->routes[] = new Route($routeData);
    }

    /**
     *  Match current path
     * @param string $pathInfo
     *
     * @return array
     *
     * @throws \Exception
     */
    function match($pathInfo)
    {
        foreach ($this->routes as $route) {
            /** @var Route $route */
            if (preg_match($route->getLinkPattern(), $pathInfo, $matches, PREG_OFFSET_CAPTURE)) {
                foreach ($matches as $key => $match) {
                    if (is_int($key)) {
                        continue;
                    }
                    $parameters[$key] = $match[0];
                }
                $parameters['controller'] = $route->getController();
                $parameters['method'] = $route->getMethod();

                return $parameters;
            }
        }

        throw new \Exception('Current route not found.');
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

}
