<?php

namespace Component;

/**
 * Class Route
 *
 * @package Component
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class Route
{

    /** @var string */
    private $linkPattern;
    /** @var string */
    private $controller;
    /** @var string */
    private $method;

    /**
     * Route constructor.
     *
     * @param $routeData
     */
    public function __construct($routeData)
    {
        if (empty($routeData)) {
            return;
        }

        $this->linkPattern = $this->createLinkPattern($routeData['linkPattern']);
        $this->controller = $routeData['controller'];
        $this->method = $routeData['method'];
    }

    /**
     * @return string
     */
    public function getLinkPattern(): string
    {
        return $this->linkPattern;
    }

    /**
     * @param string $linkPattern
     */
    public function setLinkPattern(string $linkPattern)
    {
        $this->linkPattern = $linkPattern;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     */
    public function setController(string $controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    /**
     * @param $stringPattern
     *
     * @return string
     */
    private function createLinkPattern($stringPattern)
    {
        $stringPattern = preg_replace("/\{(\w+)\}/", "(?<\$1>\w+)", $stringPattern);

        return '|^'.$stringPattern.'$|';
    }
}
