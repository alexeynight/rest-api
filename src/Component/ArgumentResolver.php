<?php

namespace Component;

/**
 * Class ArgumentResolver
 *
 * @package Component
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class ArgumentResolver
{

    /**
     * @param \Component\Request $request
     * @param                    $controller
     *
     * @return array
     */
    public function getArguments(Request $request, $controller)
    {
        $parameters = $this->getParameters($controller[0], $controller[1]);

        return $this->doGetArguments($request, $parameters);
    }

    /**
     * @param $controller
     * @param $methodName
     *
     * @return \ReflectionParameter[]
     */
    private function getParameters($controller, $methodName)
    {
        $r = new \ReflectionMethod($controller, $methodName);

        return $r->getParameters();
    }

    /**
     * @param \Component\Request $request
     * @param                    $parameters
     *
     * @return array
     */
    private function doGetArguments(Request $request, $parameters)
    {
        $arguments = [];
        $attributes = array_merge((array)$request->getAttributes(), (array)$request->getRequest());

        foreach ($parameters as $param) {
            /** @var \ReflectionParameter $param */
            $paramName = $param->getName();
            if (array_key_exists($paramName, $attributes)) {
                //resolve scalar parameters
                $arguments[$paramName] = $attributes[$paramName];
            } elseif ((string)$param->getType() == (string)$request) {
                // resolve Request injection
                $arguments[$paramName] = $request;
            }
        }

        return $arguments;
    }

}
