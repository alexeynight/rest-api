<?php

namespace Component;

/**
 * Class JsonResponse
 *
 * @package Component
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class JsonResponse extends Response
{

    /** @var string */
    private $data;

    /**
     * JsonResponse constructor.
     *
     * @param string $content
     * @param int    $status
     * @param array  $headers
     */
    public function __construct($content = '', $status = self::HTTP_OK, array $headers = [])
    {
        parent::__construct('', $status, $headers);

        $this->addHeader('Content-Type', 'application/json');
        // https://www.owasp.org/index.php/AJAX_Security_Cheat_Sheet#Always_return_JSON_with_an_Object_on_the_outside
        $this->data = json_encode([$content]);

        $this->setContent($this->data);
    }
}
