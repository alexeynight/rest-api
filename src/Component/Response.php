<?php

namespace Component;

/**
 * Class Response
 *
 * @package Component
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
/**
 * Class Response
 *
 * @package Component
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class Response
{

    const HTTP_OK = 200;
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_PAYMENT_REQUIRED = 402;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;

    /**
     * @var array
     */
    public $headers = [];

    /**
     * @var string
     */
    protected $content;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @var string
     */
    protected $statusText;

    public static $statusTexts = [
        200 => 'OK',
        301 => 'Moved Permanently',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
    ];

    /**
     * Response constructor.
     *
     * @param string $content
     * @param int    $status
     * @param array  $headers
     */
    public function __construct($content = '', $status = self::HTTP_OK, $headers = [])
    {
        $this->setContent($content);
        $this->setStatusCode($status);
    }

    /**
     * Factory method
     *
     * @param string $content
     * @param int    $status
     * @param array  $headers
     *
     * @return static
     */
    public static function create($content = '', $status = self::HTTP_OK, $headers = [])
    {
        return new static($content, $status, $headers);
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function addHeader(string $key, string $value)
    {
        $this->headers[$key] = $value;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     *
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Sets the response content
     *
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return mixed|string
     */
    public function getStatusText()
    {
        return self::$statusTexts[$this->statusCode] ?? '';
    }
}
