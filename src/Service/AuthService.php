<?php

namespace Service;

use Component\Request;

/**
 * Class AuthService
 *
 * @package Service
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class AuthService extends BaseService
{

    const DATA_FILE = 'auth_data.csv';

    /**
     * Login checking
     *
     * @param Request $request
     *
     * @return bool
     */
    public function isLoggedIn(Request $request): bool
    {
        if (!property_exists($request->getServer(), 'PHP_AUTH_USER') or !property_exists($request->getServer(), 'PHP_AUTH_PW')) {
            return false;
        }

        return $this->tryLogin($request->getServer()->{'PHP_AUTH_USER'}, $request->getServer()->{'PHP_AUTH_PW'});
    }

    /**
     * @param $username
     * @param $password
     *
     * @return bool
     */
    private function tryLogin($username, $password): bool
    {
        $authData = $this->getData();

        return (array_key_exists($username, $authData) && md5($password) == $authData[$username]['passwordHash']);
    }
}
