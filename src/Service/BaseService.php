<?php

namespace Service;

use Component\DataProvider;

/**
 * Class BaseService
 *
 * @package Service
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
abstract class BaseService
{

    const DATA_FILE = '';

    /** @var DataProvider */
    private $dataProvider;

    /**
     * UserService constructor.
     */
    public function __construct()
    {
        $this->dataProvider = new DataProvider();
    }

    /**
     * @return array
     */
    protected function getData(): array
    {
        return $this->dataProvider->getData(static::DATA_FILE);
    }
}
