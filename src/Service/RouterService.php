<?php

namespace Service;

use Component\Router;

/**
 * Class RouterService
 *
 * @package Service
 *
 * @author  Alexey Fedko <alexey.night@gmail.com>
 */
class RouterService extends BaseService
{

    const DATA_FILE = 'routes_data.csv';

    /**
     * Create Router instance and fill it
     *
     * @return Router
     */
    public function createRouter(): Router
    {
        $router = new Router();
        $routesData = $this->getData();

        foreach ($routesData as $route) {
            $router->add($route);
        }

        return $router;
    }
}
