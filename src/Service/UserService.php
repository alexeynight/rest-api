<?php

namespace Service;

use Component\DataProvider;

/**
 * Provides Users data
 *
 * @author Alexey Fedko <alexey.night@gmail.com>
 */
class UserService extends BaseService
{

    const DATA_FILE = 'users_data.csv';

    /**
     * @param $userId
     *
     * @return mixed|null
     */
    public function getUserById($userId)
    {
        $users = $this->getData();

        return array_key_exists($userId, $users) ? $users[$userId] : null;
    }

    /**
     * @return array
     */
    public function getAllUsers(): array
    {
        return $this->getData();
    }

}
