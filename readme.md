# REST API test project

This is test REST API without any frameworks (pure PHP).

## Requirements

You will need PHP 7.1 or above.

## How to use

### Step 1. Server configuration

You will want to launch the server with the command:

```
php -S localhost:8000
```

This will allow you to access the path:

```
http://localhost:8000
```

### Step 2. Login

Use HTTP Basic Auth to authenticate clients. Plz use predefined credentials:

```
login:user2
pass:qwerty
```

Or you can use Cookie:

```
Authorization: Basic dXNlcjI6cXdlcnR5
```

### Step 3. Retrive data

You can ��� full list of users:

```
http://localhost:8000/api/users/
```

For access to single user data plz follow url:

```
http://localhost:8000/api/user/{id}/
```

Where `{id}` is user ID (from 1 to 99 in the test data). For example:

```
http://localhost:8000/api/user/1/
http://localhost:8000/api/user/17/
http://localhost:8000/api/user/89/
```

## How to modify data

### Add/Delete/Modify credentials

You can edit file `./data/auth_data.csv`.

### Create/Delete/Modify routes

You can edit file `./data/routes_data.csv`.

### Add/Delete/Modify test data (user list)

You can edit file `./data/users_data.csv`.